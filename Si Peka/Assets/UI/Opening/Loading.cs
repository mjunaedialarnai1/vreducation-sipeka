﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    public void LoadScene()
    {
        if (PlayerPrefs.GetInt("opening") == 0)
        {
            SceneManager.LoadScene(1);
        }
        else
        {
            SceneManager.LoadScene(2);
        }
    }

    public void sceneStorySudahDibuka()
    {
        if (PlayerPrefs.GetInt("opening") == 0)
        {
            PlayerPrefs.SetInt("opening", 1);
        }    
    }
}
