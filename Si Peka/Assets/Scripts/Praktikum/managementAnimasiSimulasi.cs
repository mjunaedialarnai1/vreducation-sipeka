﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class managementAnimasiSimulasi : MonoBehaviour
{
    public Text TextInfoAlat;
    public GameObject animasiVideoPlayer, VideoPlayerAsli,tombolPlayVideo,tombolPauseVideo,tombolInfoDanLainnya;
    public Animator anim;
    public int animController=0;

    public VideoClip[] videoPenjelasan;
    public VideoPlayer videoPlayer;
    public GameObject[] keterangan;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!videoPlayer.isPlaying)
        {
            tombolPlayVideo.SetActive(true);
            tombolPauseVideo.SetActive(false);
        }
        else
        {
            tombolPauseVideo.SetActive(true);
            tombolPlayVideo.SetActive(false);
        }
    }

    public void aktifkanAnimasiIdle()
    {
        anim.SetBool("idlePlay", true);
    }

    

    public void nextKlik()
    {
        animController++;
        PlaykumpulanAnimasi();
        videoPlayer.Stop();
        animasiVideoPlayer.SetActive(true);
        VideoPlayerAsli.SetActive(false);
        if (animController > 9)
        {
            animController = 1;
        }
    }
    public void PrevKlik()
    {
        animController--;
        PlaykumpulanAnimasi();
        videoPlayer.Stop();
        animasiVideoPlayer.SetActive(true);
        VideoPlayerAsli.SetActive(false);
        if (animController < 1)
        {
            animController = 9;
        }
    }
    public void PlayVideo()
    {
        videoPlayer.Play();
        VideoPlayerAsli.SetActive(true);
        animasiVideoPlayer.SetActive(false);
        
    }
    public void PauseVideo()
    {
        videoPlayer.Pause();
    }

    public void PlaykumpulanAnimasi()
    {
        if (animController != 0)
        {
            tombolInfoDanLainnya.SetActive(true);
        }

        if (animController == 1)
        {
            anim.Play("1Wearpack");
            TextInfoAlat.text = "WEARPACK";
            videoPlayer.clip = videoPenjelasan[0];
            foreach(GameObject a in keterangan)
            {
                a.SetActive(false);
            }
            keterangan[0].SetActive(true);

        }
        else if (animController == 2)
        {
            anim.Play("2Masker");
            TextInfoAlat.text = "MASKER";
            videoPlayer.clip = videoPenjelasan[1];
            foreach (GameObject a in keterangan)
            {
                a.SetActive(false);
            }
            keterangan[1].SetActive(true);
        }
        else if (animController == 3)
        {
            anim.Play("3Kacamata");
            TextInfoAlat.text = "KACAMATA";
            videoPlayer.clip = videoPenjelasan[2];
            foreach (GameObject a in keterangan)
            {
                a.SetActive(false);
            }
            keterangan[2].SetActive(true);
        }
        else if (animController == 4)
        {
            anim.Play("4SarungTangan");
            TextInfoAlat.text = "SARUNG TANGAN";
            videoPlayer.clip = videoPenjelasan[3];
            foreach (GameObject a in keterangan)
            {
                a.SetActive(false);
            }
            keterangan[3].SetActive(true);
        }
        else if (animController == 5)
        {
            anim.Play("5Topi");
            TextInfoAlat.text = "TOPI";
            videoPlayer.clip = videoPenjelasan[4];
            foreach (GameObject a in keterangan)
            {
                a.SetActive(false);
            }
            keterangan[4].SetActive(true);
        }
        else if (animController == 6)
        {
            anim.Play("6Earmuffs");
            TextInfoAlat.text = "EARMUFFS";
            videoPlayer.clip = videoPenjelasan[5];
            foreach (GameObject a in keterangan)
            {
                a.SetActive(false);
            }
            keterangan[5].SetActive(true);
        }
        else if (animController == 7)
        {
            anim.Play("7RompiSafety");
            TextInfoAlat.text = "ROMPI SAFETY";
            videoPlayer.clip = videoPenjelasan[6];
            foreach (GameObject a in keterangan)
            {
                a.SetActive(false);
            }
            keterangan[6].SetActive(true);
        }
        else if (animController == 8)
        {
            anim.Play("8SabukPengaman");
            TextInfoAlat.text = "SABUK PENGAMAN";
            videoPlayer.clip = videoPenjelasan[7];
            foreach (GameObject a in keterangan)
            {
                a.SetActive(false);
            }
            keterangan[7].SetActive(true);
        }
        else if (animController == 9)
        {
            anim.Play("9Boots");
            TextInfoAlat.text = "BOOTS";
            videoPlayer.clip = videoPenjelasan[8];
            foreach (GameObject a in keterangan)
            {
                a.SetActive(false);
            }
            keterangan[8].SetActive(true);
        }
    }

}
