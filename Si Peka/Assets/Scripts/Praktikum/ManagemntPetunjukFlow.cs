﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagemntPetunjukFlow : MonoBehaviour
{
    public Text textInformasi;
    public GameObject informasi, kamuSudah,lihatSimulasi,kotakDialog;
    public Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("flowMenggunakanAPD", 0);
    }

    public void openInformasi()
    {
        informasi.SetActive(true);
        kamuSudah.SetActive(false);
        Debug.Log("JADI!");
        anim.Play("animasiflowAR");
    }

    /*0.	Wearpack 
1.	Rompi safety
2.	Sabuk pengaman
3.	Sepatu boot
4.	Sarung tangan
5.	Masker
6.	Kacamata
7.	ear muffs
8.	safety helm*/

    // Update is called once per frame

    void Update()
    {
        if (PlayerPrefs.GetInt("flowMenggunakanAPD") == 0)
        {
            textInformasi.text = "Pakailah WearPack";
        }else if(PlayerPrefs.GetInt("flowMenggunakanAPD") == 1)
        {
            textInformasi.text = "Pakailah Rompi safety";
        }
        else if (PlayerPrefs.GetInt("flowMenggunakanAPD") == 2)
        {
            textInformasi.text = "Pakailah Sabuk pengaman";
        }
        else if (PlayerPrefs.GetInt("flowMenggunakanAPD") == 3)
        {
            textInformasi.text = "Pakailah Sepatu boot";
        }
        else if (PlayerPrefs.GetInt("flowMenggunakanAPD") == 4)
        {
            textInformasi.text = "Pakailah sarung tangan";
        }
        else if (PlayerPrefs.GetInt("flowMenggunakanAPD") == 5)
        {
            textInformasi.text = "Pakailah Masker";
        }
        else if (PlayerPrefs.GetInt("flowMenggunakanAPD") == 6)
        {
            textInformasi.text = "Pakailah Kacamata";
        }
        else if (PlayerPrefs.GetInt("flowMenggunakanAPD") == 7)
        {
            textInformasi.text = "Pakailah Ear Muffs";
        }
        else if (PlayerPrefs.GetInt("flowMenggunakanAPD") == 8)
        {
            textInformasi.text = "Pakailah safety helm";
        }
        else if (PlayerPrefs.GetInt("flowMenggunakanAPD") == 9)
        {
            textInformasi.text = "Kamu telah memakai semua alat pelindung K3. Tekan tombol buka untuk membuka simulasi Animasi perlengkapan k3 kontruksi";
            PlayerPrefs.SetInt("ManagementButton", 2);
           // lihatSimulasi.SetActive(true);
            //kotakDialog.SetActive(false);
        }
        else 
        {
            textInformasi.text = "Kamu telah memakai semua alat pelindung K3. Tekan tombol buka untuk membuka simulasi alat k3 kontruksi";
        }
    }
}
