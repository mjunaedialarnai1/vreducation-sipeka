﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StorymanagementScript : MonoBehaviour
{
    public InputField inputUserName;
    public GameObject TxtBox1, TxtBox2, TxtBox3;
    // Start is called before the first frame update

    private void Start()
    {
        string userName=PlayerPrefs.GetString("UserName");
        if (!string.IsNullOrEmpty(userName))
        {
            TxtBox3.SetActive(true);
            TxtBox2.SetActive(false);
            TxtBox1.SetActive(false);
        }
        else
        {
            TxtBox2.SetActive(false);
            TxtBox1.SetActive(true);
            TxtBox3.SetActive(false);
        }
    }

    public void setUserName()
    {
        string playername = inputUserName.text;
        if (!string.IsNullOrEmpty(playername))
        {
            PlayerPrefs.SetString("UserName", playername);
       
            //Debug.Log(playername);
            TxtBox2.SetActive(false);
            TxtBox3.SetActive(true);
        }
    }

    public void ResetUserName()
    {
        PlayerPrefs.DeleteKey("UserName");
    }
}
