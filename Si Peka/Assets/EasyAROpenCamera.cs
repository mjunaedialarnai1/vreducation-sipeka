﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;


public class EasyAROpenCamera : MonoBehaviour
{
    GameObject dialog = null;
    // Start is called before the first frame update
    void Start()
    {

        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            Permission.RequestUserPermission(Permission.Camera);
            dialog = new GameObject();
            
        }

    }
    void OnGUI()
    {

        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            // The user denied permission to use the microphone.
            // Display a message explaining why you need it with Yes/No buttons.
            // If the user says yes then present the request again
            // Display a dialog here.
            dialog.AddComponent<PermissionsRationaleDialog>();
            return;
        }
        else if (dialog != null)
        {
            Destroy(dialog);
        }


        // Now you can do things with the microphone
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
